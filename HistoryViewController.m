//
//  HistoryViewController.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 26/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"
#import "DetailViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "ViewController.h"
#import "ChatViewController.h"
#import "HistoryInTellBox+CoreDataProperties.h"
#import "constant.h"
#import "UIImageView+WebCache.h"


@interface HistoryViewController ()
{
    NSArray *dictArray,*fetchedObjects;
    NSMutableDictionary *dictForTV;
    MBProgressHUD *hud;
    NSString * historyURL,*imageURLHist;
}
@end
NSManagedObjectContext *historyContext;

@implementation HistoryViewController

#pragma mark ViewLifeCycle method

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];

    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"History"];
    [self.tableview registerNib:[UINib nibWithNibName:@"HistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    historyContext=[UIAppDelegate managedObjectContext];
    [self loadingElementsInHistory];
    
    
    
    
}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInHistory];
        
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark LoadingData method

-(void)loadingElementsInHistory{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        dictArray = [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"Email id in history %@",emailIdString);
        historyURL = [NSString stringWithFormat:@"getuserrecord?device_id=%@&app_name=%@&email_id_to=%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI,emailIdString];
        
        
        
        
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        
        [manager GET:historyURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"response %@",responseObject);
            
             if ([responseObject objectForKey:@"response"]) {
                 dictArray = [responseObject objectForKey:@"response"];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                     [self.tableview reloadData];
                     
                 });
                 
                 
                 NSLog(@"Count in Online Service %lu",(unsigned long)dictArray.count);
                 unsigned long i=[self fetchingCoreData];
                 NSLog(@"Count %lu",i);
                 
                 if (dictArray.count<i || dictArray.count>i) {
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         [self deleteAllObjects:@"HistoryInTellBox"];
                         [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             
                             [historyContext performBlockAndWait:^{
                                 
                                 HistoryInTellBox * history = [NSEntityDescription insertNewObjectForEntityForName:@"HistoryInTellBox"inManagedObjectContext:historyContext];
                                 history.msgdetail = [obj objectForKey:@"msg_detail"];
                                 history.locationdetail = [obj objectForKey:@"location_detail"];
                                 history.createddate = [obj objectForKey:@"created_date"];
                                 history.changeitid = [obj objectForKey:@"changeit_id"];
                                 if ([[obj objectForKey:@"images"]isEqualToString:@""]) {
                                     //[history setValue:@"noImage" forKey:@"image"];
                                     
                                     history.image=@"noImage";
                                 }
                                 else{
                                     //[history setValue:[obj objectForKey:@"images"] forKey:@"image"];
                                     history.image=[obj objectForKey:@"images"];
                                 }
                                 
                             }];
                             
                             NSError *error;
                             
                             if (![historyContext save:&error]) {
                                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                             }
                             
                         }];
                         
                     });
                     
                 }
             }
             else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                 });
                 [self deleteAllObjects:@"HistoryInTellBox"];
                 
             }
            

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                
            });
            [self deleteAllObjects:@"HistoryInTellBox"];
            

            
        }];

        
        
    }
    else{
        // Test listing all Stored Messages from the DB
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        
        [historyContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryInTellBox" inManagedObjectContext:historyContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                                initWithKey:@"createddate" ascending:NO];
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            dictArray = [historyContext executeFetchRequest:fetchRequest error:&error];
            NSLog(@"Count in coredata %lu",(unsigned long)dictArray.count);
        }];
        
        
    }
}

#pragma mark FetchingCoreData method

-(unsigned long)fetchingCoreData{
    __block NSArray * coreDataArray;
    [historyContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryInTellBox" inManagedObjectContext:historyContext];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        coreDataArray = [historyContext executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    
    return coreDataArray.count;
}

#pragma mark Deleting Coredata method
- (void) deleteAllObjects: (NSString *) entityDescription  {
    
    [historyContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:historyContext];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [historyContext executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        
        for (NSManagedObject *managedObject in items) {
            [historyContext deleteObject:managedObject];
            NSLog(@"%@ object deleted",entityDescription);
        }
        if (![historyContext save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
        
    }];
    
    
}

#pragma mark Gesture method

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark TableView Delegat methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[HistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    //HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.tag = indexPath.row;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {


    dictForTV = [dictArray objectAtIndex:indexPath.row];
    
        if ([[dictForTV objectForKey:@"images"] isEqualToString:@""]) {
            
            UIImage *tmpImage = [UIImage imageNamed:@"ImgNotFound.png"];
            cell.imagevw.image = tmpImage;
            
        }
        else{
            cell.imagevw.image=nil;
            
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator setCenter: cell.imagevw.center];
            [activityIndicator startAnimating];
            [cell.contentView addSubview:activityIndicator];
            
            //imageURLHist =[[NSString stringWithFormat:@"%@%@",kImageURL,[dictForTV objectForKey:@"images"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
           imageURLHist= [[NSString stringWithFormat:@"%@%@",kImageURL,[dictForTV objectForKey:@"images"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageURLHist].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:imageURLHist] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }

            
            
            
        }

    cell.dataText.text = [dictForTV objectForKey:@"msg_detail"];
    cell.textOfLocation.text = [dictForTV objectForKey:@"location_detail"];
    cell.dateAndTime.text = [dictForTV objectForKey:@"created_date"];
    
    cell.myButton.tag= [[dictForTV objectForKey:@"changeit_id"] integerValue];
    [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendQueryButton.tag=[[dictForTV objectForKey:@"changeit_id"] integerValue];
        [cell.sendQueryButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        HistoryInTellBox *info = [dictArray objectAtIndex:indexPath.row];
        //cell.textLabel.text = obj. name;
        // Configure the cell...
        //dict = [fetchedObjects objectAtIndex:indexPath.row];
        cell.dataText.text = info.msgdetail;
        cell.textOfLocation.text = info.locationdetail;
        cell.dateAndTime.text = info.createddate;
        NSLog(@"Message %@",info.msgdetail);
        
        cell.myButton.tag= [info.changeitid integerValue];
        [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendQueryButton.tag=[info.changeitid integerValue];
        [cell.sendQueryButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSLog(@"Info Image %@",[info valueForKey:@"image"]);
        if (![[info valueForKey:@"image"] isEqualToString:@"noImage"])
        {
            // cell.imagevw.image  = [UIImage imageWithData:info.image];
            cell.imagevw.image=nil;
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator setCenter: cell.imagevw.center];
            [activityIndicator startAnimating];
            [cell.contentView addSubview:activityIndicator];
            imageURLHist =[NSString stringWithFormat:@"%@%@",kImageURL, [info valueForKey:@"image"]];
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageURLHist].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:imageURLHist] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }
            
            
        }

        else{
            cell.imagevw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }

        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    // hud.dimBackground = YES;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            
                DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
                
                NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
//                NSString * imageString=[NSString stringWithFormat:@"%@%@",kImageURL, [rowDict objectForKey:@"images"]];
//                NSURL * url=[NSURL URLWithString:imageString];
                
                
                if ([[rowDict objectForKey:@"images"] isEqualToString:@""]) {
                    detail.imageName=@"noImage";
                }
                else{
                    detail.imageName = [rowDict objectForKey:@"images"];
                    
                }
                //NSLog(@"imagename%@Hello",imageString);
                NSLog(@"RowDict %@",rowDict);

                
                detail.date = [rowDict objectForKey:@"created_date"];
                detail.details = [rowDict objectForKey:@"msg_detail"];
                detail.locationStr=[rowDict objectForKey:@"location_detail"];
            dispatch_async(dispatch_get_main_queue(), ^{

                [self.navigationController pushViewController:detail animated:YES];
                
                
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
            
        });
        
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
           
                
                DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
                HistoryInTellBox *info = [dictArray objectAtIndex:indexPath.row];
                //NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
                detail.imageName = info.image;
                detail.date = info.createddate;
                detail.details = info.msgdetail;
                detail.locationStr=info.locationdetail;
             dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:detail animated:YES];
                
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
            
        });

       
       
        
        
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

#pragma mark Resend & SendQueryMethods


-(void)sendQueryButtonClick:(id)sender
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIButton *senderButton = (UIButton *)sender;
        
        //ChatViewController * chatController=[[ChatViewController alloc]init];
        ChatViewController * chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
        chatController.itemID=[NSString stringWithFormat:@"%ld",(long)senderButton.tag];
        NSLog(@"sender button %@",chatController.itemID);
        //[self presentViewController:chatController animated:YES completion:nil];
        [self.navigationController pushViewController:chatController animated:YES];
    });
    
}

-(void)resendButtonClick:(id)sender{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Are you sure to Resend?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * resendAction=[UIAlertAction actionWithTitle:@"Resend" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
            hud.contentColor =khudColour;
            hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
            //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
            
            // Set the label text.
            hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");
            UIButton *senderButton = (UIButton *)sender;
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            // getting an NSString
            NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
            
            NSString *resendStr = [NSString stringWithFormat:@"resendemailbyid?changeit_id=%ld&app_name=%@&email_id_to=%@",(long)(senderButton.tag),kAppNameAPI,emailIdString];
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            [manager POST:resendStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"Response %@",responseObject);
                 if ([[responseObject objectForKey:@"response"]isEqualToString:@"Email sent successfully"]) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [hud hideAnimated:YES];
                     });
                     [self errorAlertWithTitle:kAppNameAlert message:@"Email Resend Successfully.If you don’t receive a confirmation email within 1 hour please contact us" actionTitle:@"Done"];
                 }
                 else{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [hud hideAnimated:YES];
                     });
                     [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
                     
                 }
                
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"Error %@",error);
               
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                    [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];

               
                
            }];

        }];
        UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil];
        
        [alert addAction:resendAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
    }

}


-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


@end
