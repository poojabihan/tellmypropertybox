//
//  HistoryInTellBox+CoreDataProperties.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryInTellBox+CoreDataProperties.h"

@implementation HistoryInTellBox (CoreDataProperties)

+ (NSFetchRequest<HistoryInTellBox *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HistoryInTellBox"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic image;
@dynamic locationdetail;
@dynamic msgdetail;

@end
