//
//  ChatImageViewController.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 02/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImageViewController : UIViewController<UIGestureRecognizerDelegate>
@property NSString * imageURL;
@property (weak, nonatomic) IBOutlet UIImageView *chatImage;

@end
