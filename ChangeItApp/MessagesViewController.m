//
//  MessagesViewController.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageItemTableViewCell.h"
#import "ChatViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "MessagesInTellBox+CoreDataProperties.h"
#import "ChatInTellBox+CoreDataProperties.h"
#import "constant.h"
#import <MBProgressHUD.h>



@interface MessagesViewController ()
{
    NSArray * dictArray,*itemChatArray,*resultArray;
    NSMutableDictionary *dictTVInbox;
    //UIRefreshControl *refreshControl;
    MessageItemTableViewCell * messageItemCell;
    BOOL flag;
    MBProgressHUD * hud;
    
    
}
@end
NSManagedObjectContext *messageContext, * chatContextMessage;
;

@implementation MessagesViewController

#pragma mark ViewLifeCycle method

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    flag=YES;
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:@"Messages"];
    
    
    [self.messagesTableView registerNib:[UINib nibWithNibName:@"MessageItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"messageCell"];
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    messageContext=[UIAppDelegate managedObjectContext];
    chatContextMessage=[UIAppDelegate managedObjectContext];
    
    self.messagesTableView.dataSource = self;
    self.messagesTableView.delegate = self;
//    refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
//    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//    
//    [self.messagesTableView addSubview:refreshControl];

}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        
        [self loadingElementsInInboxWithBackgroundThread];
        
        
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    //[self loadingElementsInMessage];
    if (flag==YES) {
        [self loadingElementsInMessage];
        flag=NO;
    }
    else{
        //[self performSelectorInBackground:@selector(loadingElementsInInboxWithBackgroundThread) withObject:nil];
        [self loadingElementsInInboxWithBackgroundThread];
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
    
    
}
#pragma mark LoadingData method

-(void)loadingElementsInMessage{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSString *str;
        dictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            str= [NSString stringWithFormat:@"getinboxitem?email_id=&app_name=%@",kAppNameAPI];
        }
        else{
            str = [NSString stringWithFormat:@"getinboxitem?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
        }
        
        
        
        
        
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"dictionary response %@",responseObject);
            if ([responseObject objectForKey:@"response"]) {
                dictArray = [responseObject objectForKey:@"response"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.messagesTableView reloadData];
                });
                NSLog(@"Array %@",dictArray);
                NSLog(@"Count of Array %lu",(unsigned long)dictArray.count);
                NSArray * arr;
                BOOL foo = true;
                unsigned long j=[self fetchingCoreDataForMessage];
                for (NSMutableDictionary *dictInFor in dictArray) {
                    arr=[dictInFor objectForKey:@"itemchat"];
                    //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                    unsigned long i=[self fetchingCoreDataForChat:[dictInFor objectForKey:@"changeit_id"]];
                    if (i<arr.count ||i>arr.count|| j<dictArray.count||j>dictArray.count) {
                        foo=false;
                    }
                    
                }
                NSLog(@"Inbox Wow %d ",foo);
                if (foo==false) {
                    
                    [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
                    [self deleteAllObjectsInChat:@"ChatInTellBox"];
                    [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        [messageContext performBlockAndWait:^{
                            MessagesInTellBox * inbox = [NSEntityDescription insertNewObjectForEntityForName:@"MessagesInTellBox"inManagedObjectContext:messageContext];
                            
                            inbox.lastmsg=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
                            inbox.msgdetail = [obj objectForKey:@"msg_detail"];
                            inbox.msgtype = [obj objectForKey:@"msg_type"];
                            inbox.createddate=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"];
                            inbox.changeitid =[obj objectForKey:@"changeit_id"];
                            
                            itemChatArray=[obj objectForKey:@"itemchat"];
                            //[self deleteAllObjectsInChat:@"Chat"];
                            
                            
                            
                            [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                if (chatContextMessage != nil) {
                                    [chatContextMessage performBlockAndWait:^{
                                        NSError *error;
                                        ChatInTellBox * chatCD = [NSEntityDescription insertNewObjectForEntityForName:@"ChatInTellBox"inManagedObjectContext:chatContextMessage];
                                        chatCD.changeitid =[obj objectForKey:@"changeit_id"];
                                        chatCD.createddate=[chatObj objectForKey:@"created_date"];
                                        chatCD.message = [chatObj objectForKey:@"message"];
                                        chatCD.usertype = [chatObj objectForKey:@"user_type"];
                                        chatCD.posttype=[chatObj objectForKey:@"post_type"];
                                        if (![chatContextMessage save:&error]) {
                                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                        }
                                        
                                        
                                    }];
                                }
                                
                                
                                
                            }];
                            
                            
                            
                            NSError *error;
                            
                            
                            if (![messageContext save:&error]) {
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                            }
                        }];
                    }];
                    
                    
                    
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    //[refreshControl endRefreshing];
                });
                
                [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
                [self deleteAllObjectsInChat:@"ChatInTellBox"];
            }
            
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                //[refreshControl endRefreshing];
            });
            [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
            [self deleteAllObjectsInChat:@"ChatInTellBox"];
            
            

            
        }];
        
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            //[refreshControl endRefreshing];
        });
        
        [messageContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesInTellBox" inManagedObjectContext:messageContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
            
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            
            dictArray = [messageContext executeFetchRequest:fetchRequest error:&error];
        }];
        
    }
    
}

-(void)loadingElementsInInboxWithBackgroundThread{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus != NotReachable)
        {
           
            NSString *str;
            dictArray = [[NSArray alloc]init];
            itemChatArray= [[NSArray alloc]init];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            // getting an NSString
            NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
            NSLog(@"email id in inbox %@",emailIdString);
            if (emailIdString==NULL) {
                str= [NSString stringWithFormat:@"getinboxitem?email_id=&app_name=%@",kAppNameAPI];
            }
            else{
                str = [NSString stringWithFormat:@"getinboxitem?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
            }
            
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            dispatch_queue_t myQueue = dispatch_queue_create("com.Chetaru.TellDan.loadingElementsInInboxWithBackgroundThread", DISPATCH_QUEUE_SERIAL);
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            [manager setCompletionQueue:myQueue];
            
            
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                if ([responseObject objectForKey:@"response"]) {
                    dictArray = [responseObject objectForKey:@"response"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.messagesTableView reloadData];
                        //[activity stopAnimating];
                        // [self.view setUserInteractionEnabled:YES];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        
                    });
                    
                    
                    NSArray * arr;
                    BOOL foo = true;
                    unsigned long j=[self fetchingCoreDataForMessage];
                    for (NSMutableDictionary *dictInFor in dictArray) {
                        arr=[dictInFor objectForKey:@"itemchat"];
                        //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                        unsigned long i=[self fetchingCoreDataForChat:[dictInFor objectForKey:@"changeit_id"]];
                        if (i<arr.count ||i>arr.count|| j<dictArray.count||j>dictArray.count) {
                            foo=false;
                        }
                        
                    }
                    NSLog(@"Inbox Wow %d ",foo);
                    if (foo==false) {
                        
                        [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
                        [self deleteAllObjectsInChat:@"ChatInTellBox"];
                        [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            [messageContext performBlockAndWait:^{
                                MessagesInTellBox * inbox = [NSEntityDescription insertNewObjectForEntityForName:@"MessagesInTellBox"inManagedObjectContext:messageContext];
                                
                                inbox.lastmsg=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
                                inbox.msgdetail = [obj objectForKey:@"msg_detail"];
                                inbox.msgtype = [obj objectForKey:@"msg_type"];
                                inbox.createddate=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"];
                                inbox.changeitid =[obj objectForKey:@"changeit_id"];
                                
                                itemChatArray=[obj objectForKey:@"itemchat"];
                                //[self deleteAllObjectsInChat:@"Chat"];
                                
                                
                                
                                [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    
                                    if (chatContextMessage != nil) {
                                        [chatContextMessage performBlockAndWait:^{
                                            NSError *error;
                                            ChatInTellBox * chatCD = [NSEntityDescription insertNewObjectForEntityForName:@"ChatInTellBox"inManagedObjectContext:chatContextMessage];
                                            chatCD.changeitid =[obj objectForKey:@"changeit_id"];
                                            chatCD.createddate=[chatObj objectForKey:@"created_date"];
                                            chatCD.message = [chatObj objectForKey:@"message"];
                                            chatCD.usertype = [chatObj objectForKey:@"user_type"];
                                            chatCD.posttype=[chatObj objectForKey:@"post_type"];
                                            if (![chatContextMessage save:&error]) {
                                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                            }
                                            
                                            
                                        }];
                                    }
                                    
                                    
                                    
                                }];
                                
                                
                                
                                NSError *error;
                                
                                
                                if (![messageContext save:&error]) {
                                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    });
                                }
                            }];
                        }];
                        
                        
                        
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[activity stopAnimating];
                        //[self.view setUserInteractionEnabled:YES];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                    [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
                    [self deleteAllObjectsInChat:@"ChatInTellBox"];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"error = %@", error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[activity stopAnimating];
                    //[self.view setUserInteractionEnabled:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                [self deleteAllObjectsInMessage:@"MessagesInTellBox"];
                [self deleteAllObjectsInChat:@"ChatInTellBox"];
            }];
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            [messageContext performBlockAndWait:^{
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesInTellBox" inManagedObjectContext:messageContext];
                [fetchRequest setEntity:entity];
                NSError *error;
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
                
                [fetchRequest setSortDescriptors:@[sortDescriptor]];
                
                
                dictArray = [messageContext executeFetchRequest:fetchRequest error:&error];
            }];
        }
  
    
}

#pragma mark PullToRefresh Method


-(void)refreshData{
    [self loadingElementsInMessage];
}

#pragma mark Fetching Coredata Methods

-(unsigned long)fetchingCoreDataForMessage{
    __block NSArray * coreDataArray;
    [messageContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesInTellBox" inManagedObjectContext:messageContext];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        coreDataArray = [messageContext executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    return coreDataArray.count;
}

-(unsigned long)fetchingCoreDataForChat:(NSString *)changeID{
    [chatContextMessage performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ChatInTellBox" inManagedObjectContext:chatContextMessage];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"changeitid == %@", changeID]];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        resultArray = [chatContextMessage executeFetchRequest:fetchRequest error:&error];
    }];
    return resultArray.count;
    
}


#pragma mark Deleting Coredata Methods

- (void) deleteAllObjectsInMessage: (NSString *) entityDescription  {
    [messageContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:messageContext];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [messageContext executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        
        for (NSManagedObject *managedObject in items) {
            [messageContext deleteObject:managedObject];
            NSLog(@"%@ object deleted",entityDescription);
        }
        if (![messageContext save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
    }];
    
    
}

- (void) deleteAllObjectsInChat: (NSString *) entityDescription  {
    
    [chatContextMessage performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:chatContextMessage];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        NSArray *items = [chatContextMessage executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        [chatContextMessage performBlockAndWait:^{
            for (NSManagedObject *managedObject in items) {
                [chatContextMessage deleteObject:managedObject];
                NSLog(@"%@ object deleted",entityDescription);
            }
            
        }];
        if (![chatContextMessage save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
    }];
    
    
    
}

#pragma mark Gesture Method

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark TableViewDelegate Methods



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //messageItemCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    messageItemCell=[tableView dequeueReusableCellWithIdentifier:@"messageCell"];
    if (messageItemCell==nil) {
        messageItemCell=[[MessageItemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"messageCell"];
        
    }
    //[messageItemCell setSelectionStyle:UITableViewCellSelectionStyleNone];

    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        dictTVInbox = [dictArray objectAtIndex:indexPath.row];
        
        
        messageItemCell.lastMessageLabel.text=[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
        

       
        messageItemCell.titleHeadLineLabel.text = [dictTVInbox objectForKey:@"msg_detail"];
        if ([[dictTVInbox objectForKey:@"msg_type"] isEqualToString:@"ann"]) {
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"TellMyPropertyBox : %@ ",[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"]];
            
        }else{
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"TellMyPropertyBox : %@  (Item %@)",[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"],[dictTVInbox objectForKey:@"changeit_id"]];
        }
    }
    else{
        MessagesInTellBox *info = [dictArray objectAtIndex:indexPath.row];
        
        
             messageItemCell.lastMessageLabel.text=info.lastmsg;
    

       
        messageItemCell.titleHeadLineLabel.text = info.msgdetail;
        if ([info.msgtype isEqualToString:@"ann"]) {
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"TellMyPropertyBox : %@ ",info.createddate];
            
        }else{
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"TellMyPropertyBox : %@  (Item %@)",info.createddate,info.changeitid];
        }
        
    }
    
    
    return messageItemCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
        ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
        chatController.itemID=[rowDict objectForKey:@"changeit_id"];
        NSLog(@"chang id in inbox %@",[rowDict objectForKey:@"changeit_id"]);
        dispatch_async(dispatch_get_main_queue(), ^{
           [self.navigationController pushViewController:chatController animated:YES]; 
        });
        
    }
    else
    {
        MessagesInTellBox *info = [dictArray objectAtIndex:indexPath.row];
        ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
        chatController.itemID=info.changeitid;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:chatController animated:YES];
        });

        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
