//
//  ChatImageViewController.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 02/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatImageViewController.h"
#import "Reachability.h"
#import <MBProgressHUD.h>
#import "constant.h"
#import <Photos/Photos.h>
#import "ChatAsyncPhoto.h"
#import "UIImageView+WebCache.h"

@interface ChatImageViewController ()
{
    MBProgressHUD *hud;
    
    
}
@end

@implementation ChatImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    self.view.backgroundColor=[UIColor blackColor];
    UIBarButtonItem *_btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"download-32.png"] style:UIBarButtonItemStylePlain target:self action:@selector(rightButtonClick)];
    
    self.navigationItem.rightBarButtonItem=_btn;
    
    
    
    
    //UIImageView *img = [[UIImageView alloc] initWithFrame:FRAME];
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        //NSString * imageString=[[NSString stringWithFormat:@"%@%@",kChatImageURL, _imageURL]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * imageString = [[NSString stringWithFormat:@"%@%@",kChatImageURL, _imageURL]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageString].absoluteString];
            if(image == nil)
            {
                [self.chatImage sd_setImageWithURL:[NSURL URLWithString:imageString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [self.chatImage setImage:image];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [self.chatImage setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                    }
                }];
            } else {
                [self.chatImage setImage:image];
                //[activityIndicator removeFromSuperview];
            }
            
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    });
    //self.chatImage.image=[UIImage imageWithData:_imageData];
    self.chatImage.clipsToBounds = YES;
    
    self.chatImage.contentMode = UIViewContentModeScaleAspectFit;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightButtonClick{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        
        
        
        if ([self.chatImage.image isEqual:[UIImage imageNamed:@"ImgNotFound.png"]])
        {
            NSString *imgName = [self.chatImage image].accessibilityIdentifier;
            NSLog(@"Yes there is no image %@",imgName);
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Image not Available" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alertCont animated:true completion:nil];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok Action") style:UIAlertActionStyleDefault handler:nil];
                [alertCont addAction:okAction];
                
            });
        }
        else{
            
        

        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.contentColor =khudColour;
        
        // Set the label text.
        hud.label.text = NSLocalizedString(@"Saving...", @"HUD loading title");
        NSLog(@"Imageurl %@",_imageURL);
        __block PHAssetCollection *assetCollection;
        
        __block PHFetchResult *fetchResult;
        __block PHObjectPlaceholder *assetCollectionPlaceholder;
        
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", @"TellMyPropertyBox"];
        assetCollection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                   subtype:PHAssetCollectionSubtypeAny
                                                                   options:fetchOptions].firstObject;
        
        if(!assetCollection)
        {
            
            [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
                PHAssetCollectionChangeRequest *createAlbumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:@"TellMyPropertyBox"];
                assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection;
                
            } completionHandler:^(BOOL success, NSError * _Nullable error) {
                
                if (success) {
                    
                    fetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[assetCollectionPlaceholder.localIdentifier] options:nil];
                    
                    NSLog(@"fetch fetchResult %@",fetchResult);
                    // PHAssetCollection *
                    assetCollection = fetchResult.firstObject;
                    
                    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
                        
                        PHAssetChangeRequest * createAssetRequest=[PHAssetChangeRequest creationRequestForAssetFromImage:self.chatImage.image];
                        PHAssetCollectionChangeRequest* assetRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
                        [assetRequest addAssets:@[[createAssetRequest placeholderForCreatedAsset]]];
                        [self success];
                        
                        
                    } completionHandler:^(BOOL success, NSError * _Nullable error) {
                        if (!success) {
                            NSLog(@"Error creating asset: %@", error);
                            [self failed];
                        }
                        
                    }];
                }
                else{
                    NSLog(@"didFinishRecordingToOutputFileAtURL - success for ios9");
                    
                }
            }];
            NSLog(@"Done");
        }
        else{
            
            
            [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
                PHAssetChangeRequest * createAssetRequest=[PHAssetChangeRequest creationRequestForAssetFromImage:self.chatImage.image];
                PHAssetCollectionChangeRequest* assetRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
                [assetRequest addAssets:@[[createAssetRequest placeholderForCreatedAsset]]];
                [self success];
                
            } completionHandler:^(BOOL success, NSError * _Nullable error) {
                if (!success) {
                    NSLog(@"Error creating asset: %@", error);
                    [self failed];
                }
                
            }];
        }
    }
        
}
    else{
        
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alertCont animated:true completion:nil];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", @"ok Action") style:UIAlertActionStyleCancel handler:nil];
        [alertCont addAction:okAction];
        
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}
-(void)success{
    
    //[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Image Saved To Gallery" preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok Action") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alertCont addAction:okAction];
        
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
    
}
-(void)failed{
    //[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [hud hideAnimated:YES];
        
        
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Error in saving" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alertCont animated:true completion:nil];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok Action") style:UIAlertActionStyleDefault handler:nil];
        [alertCont addAction:okAction];
    });
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
