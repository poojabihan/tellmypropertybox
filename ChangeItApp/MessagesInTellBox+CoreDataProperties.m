//
//  MessagesInTellBox+CoreDataProperties.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessagesInTellBox+CoreDataProperties.h"

@implementation MessagesInTellBox (CoreDataProperties)

+ (NSFetchRequest<MessagesInTellBox *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessagesInTellBox"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic lastmsg;
@dynamic msgdetail;
@dynamic msgtype;

@end
