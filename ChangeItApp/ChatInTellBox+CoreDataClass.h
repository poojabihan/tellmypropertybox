//
//  ChatInTellBox+CoreDataClass.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatInTellBox : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatInTellBox+CoreDataProperties.h"
