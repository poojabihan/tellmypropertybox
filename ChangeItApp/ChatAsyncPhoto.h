//
//  ChatAsyncPhoto.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 02/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import "JSQPhotoMediaItem.h"

@interface ChatAsyncPhoto : JSQPhotoMediaItem
@property (nonatomic, strong) UIImageView *asyncImageView;

- (instancetype)initWithURL:(NSURL *)URL;

@end
