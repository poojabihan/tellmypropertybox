//
//  MessageItemTableViewCell.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessageItemTableViewCell.h"

@implementation MessageItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
