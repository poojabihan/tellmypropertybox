//
//  HistoryInTellBox+CoreDataProperties.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryInTellBox+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface HistoryInTellBox (CoreDataProperties)

+ (NSFetchRequest<HistoryInTellBox *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *locationdetail;
@property (nullable, nonatomic, copy) NSString *msgdetail;

@end

NS_ASSUME_NONNULL_END
