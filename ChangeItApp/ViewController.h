//
//  ViewController.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 21/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "Webservice.h"


@interface ViewController : UIViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate, CLLocationManagerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *commentTxtView;

@property (weak, nonatomic) IBOutlet UIButton *rghtButton;

@property (weak, nonatomic) IBOutlet UITextField *locationText;

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;

@property (weak, nonatomic) IBOutlet UITextField *textField1;

- (IBAction)onHistory:(id)sender;

- (IBAction)takeAndAddPhoto:(id)sender;

- (IBAction)onSendingMail:(id)sender;

- (IBAction)sendLocation:(id)sender;
- (IBAction)deleteButtonClick:(id)sender;
- (IBAction)messagesButtonClick:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UIButton *messagesButton;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *takeAndAddButtonInstance;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewInVC;

@property NSString * stringToCheckVC,*emailURL,*itemPostURL,* URLString;
-(void)sendingMail;

@end

