//
//  MessageItemTableViewCell.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageItemTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleHeadLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
