//
//  MessagesInTellBox+CoreDataProperties.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessagesInTellBox+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessagesInTellBox (CoreDataProperties)

+ (NSFetchRequest<MessagesInTellBox *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *lastmsg;
@property (nullable, nonatomic, copy) NSString *msgdetail;
@property (nullable, nonatomic, copy) NSString *msgtype;

@end

NS_ASSUME_NONNULL_END
