//
//  ChatInTellBox+CoreDataProperties.m
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatInTellBox+CoreDataProperties.h"

@implementation ChatInTellBox (CoreDataProperties)

+ (NSFetchRequest<ChatInTellBox *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ChatInTellBox"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic message;
@dynamic usertype;
@dynamic posttype;

@end
