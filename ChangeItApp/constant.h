//
//  constant.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 03/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//demo_tellsid
//tellsid

#define kBaseURL @"http://live.thechangeconsultancy.co/tellsid/index.php/apitellmybox/"
#define kImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/my-property-box/"
#define kChatImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"

//#define kBaseURL @"http://demo.thechangeconsultancy.co/demo_tellsid/index.php/apitellmybox/"
//#define kImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/my-property-box/"
//#define kChatImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/chatimg/"

//#define kBaseURL @"http://tellsid.softintelligence.co.uk/index.php/apitellmybox/"
//#define kImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/my-property-box/"
//#define kChatImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/chatimg/"

#define kAppNameAPI @"my-property-box"
#define khudColour [UIColor colorWithRed:0/255.0f green:144/255.0f blue:210/255.0f alpha:1]
#define kAppNameAlert @"Tell My Property Box"
#define kInternetOff @"Internet Connection Required!"
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#endif /* constant_h */
