//
//  ChatInTellBox+CoreDataProperties.h
//  TellMyPropertyBox
//
//  Created by Tarun Sharma on 01/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatInTellBox+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatInTellBox (CoreDataProperties)

+ (NSFetchRequest<ChatInTellBox *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *usertype;
@property (nullable, nonatomic, copy) NSString *posttype;

@end

NS_ASSUME_NONNULL_END
